# SpringBoot - WebSocket Project 0.0.1

```
기본 클라이언트 통신 
```


## 프로젝트 환경

* Java Version : Open JDK 11
* Port : 5000


## Dependencies

```
- spring-boot-starter-web
- spring-boot-starter-websocket
- lombok
```


## 접속 Tool

```
https://chrome.google.com/webstore/search/websocket
```


## 접속

```
- ws://localhost:5000/ws/chat
```


## 참조 페이지

```
https://www.daddyprogrammer.org/post/4077/spring-websocket-chatting
```


<br>
