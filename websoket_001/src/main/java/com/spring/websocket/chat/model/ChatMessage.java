package com.spring.websocket.chat.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChatMessage {

    public enum MessageType {
        ENTER, TALK
    }

    // 메세지 타입
    private MessageType type;
    
    // 방 번호
    private String roomId;
    
    // 메시지 보낸사람
    private String sender;
    
    // 메세지
    private String message; 
}
